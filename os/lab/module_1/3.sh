#!/bin/bash

awk 'BEGIN{ FS=";"; RS="\r"; s=0; a=0; } { if( $3 == "0,5" ){ s = s + $1; a += $2; } } END{ print s, a; }' Container_Crane_Controller_Data_Set.csv