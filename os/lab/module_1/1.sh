#!/bin/bash

for number in "$@"
do
  i=2
  prime=0
  if [ $number -le 1 ]
  then
    prime=1
    while [ $i -le `sqrt $number` ] && [ $prime -eq 1 ]
    do
      if [ `expr $number % $i` -eq 0 ]
      then
        prime=0
      fi

      i=`expr $i + 1`
    done
  fi

  if [ $prime -eq 0 ]
  then
    echo $number" is Not Prime"
  else
    echo $number" is Prime"
  fi
done