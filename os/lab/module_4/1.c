#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>

char* dirpath = "/home/noel/Downloads";

void reversePath(char* dest, char* src) 
{
  strcpy(dest, "");
  char* new = src;
  int slash = 0;
  if (src[0] == '/') 
  {
    src++;
    slash = 1;
  }
  
  int first = 1;
  char* tok;
  tok = strtok(new, "/");
  
  while (tok != NULL) 
  {
    char rev[1000];
    int index = 0;
    
    for (int i = strlen(tok)-1; i >= 0; i--) 
    {
      rev[index] = tok[i];
      index++;
    }
    
    rev[index] = '\0';
    if (!first || slash) 
      strcat(dest, "/");
    
    first = 0;
    strcat(dest, rev);
    tok = strtok(NULL, "/");
  }
}

static int xmp_getattr(const char *path, struct stat *stbuf) 
{
  int res;
  char fullpath[1000];
  char s[1000];
  reversePath(s, path);
  sprintf(fullpath, "%s/%s", dirpath, s);
  res = lstat(fullpath, stbuf);
  if (res == -1) return -errno;
  return 0;
}

static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi) 
{
  char fullpath[1000];
  
  if (!strcmp(fullpath, "/")) 
  {
    path = dirpath;
    strcpy(fullpath, path);
  } 
  else 
  {
    char s[1000];
    reversePath(s, path);
    sprintf(fullpath, "%s/%s", dirpath, s);
  }
  
  DIR *dp;
  struct dirent *de;
  (void)offset;
  (void)fi;
  dp = opendir(fullpath);
  
  if (dp == NULL) 
    return -errno;
  
  while ((de = readdir(dp)) != NULL) 
  {
    struct stat st;
    memset(&st, 0, sizeof(st));
    st.st_ino = de->d_ino;
    st.st_mode = de->d_type << 12;
    char s[1000];
    reversePath(s, de->d_name);
    if (filler(buf, s, &st, 0)) break;
  }
  closedir(dp);
  return 0;
}

static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi) 
{
  char fullpath[1000];
  if (!strcmp(fullpath, "/")) 
  {
    path = dirpath;
    strcpy(fullpath, path);
  }
  else
  {
    char s[1000];
    reversePath(s, path);
    sprintf(fullpath, "%s/%s", dirpath, s);
  }
  
  int fd;
  int res;
  (void)fi;
  
  fd = open(fullpath, O_RDONLY);
  
  if (fd == -1) 
    return -errno;
  res = pread(fd, buf, size, offset);
  
  if (res == -1) 
    res = -errno;
  
  close(fd);
  return res;
}

static struct fuse_operations xmp_oper = {
  .getattr = xmp_getattr,
  .readdir = xmp_readdir,
  .read = xmp_read,
};

int main(int argc, char *argv[])
{
  umask(0);
  return fuse_main(argc, argv, &xmp_oper, NULL);
}