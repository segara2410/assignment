#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <string.h>
#include <time.h>
#include <unistd.h>


int main() 
{
  char folder_name[100];

  time_t timer;
  struct tm* tm_info;  

  timer = time(NULL);
  tm_info = localtime(&timer);

  strftime(folder_name, 100, "%Y-%m-%d_%H:%M:%S", tm_info);

  pid_t child_id;
  child_id = fork();

  int status;

  if (child_id < 0) 
    exit(EXIT_FAILURE);

  if (child_id == 0)
  {    
    char *argv[] = {"mkdir", "-p", folder_name, NULL};
    execv("/bin/mkdir", argv);
  }
  else 
  {
    while ((wait(&status)) > 0);

    char *user = getenv("USER");
    char source_folder_name[20] = "/home/";

    strcat(source_folder_name, user);
    strcat(source_folder_name, "/Music");

    char *argv[] = {"cp", "-r", source_folder_name, folder_name, NULL};
    execv("/bin/cp", argv);
  }
}