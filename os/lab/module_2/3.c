#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>

int checkFile(char* filename)
{
  struct stat buffer;
  int exist = stat(filename, &buffer);
  if (exist == 0)
    return 1;
  else
    return 0;
}

int main() {
  pid_t pid, sid;

  pid = fork();

  if (pid < 0)
    exit(EXIT_FAILURE);

  if (pid > 0) 
    exit(EXIT_SUCCESS);

  umask(0);

  sid = setsid();
  if (sid < 0)
    exit(EXIT_FAILURE);

  close(STDIN_FILENO);
  close(STDOUT_FILENO);
  close(STDERR_FILENO);

  int i = 1;

  while (i) 
  {
    char ch;
    char target_file[20];
    
    sprintf(target_file, "diary.log.%d", i);

    if (checkFile(target_file))
    {
      i++;
      continue;
    }

    char source_file[20] = "diary.txt";

    FILE *source, *target;

    source = fopen(source_file, "r");

    if (source == NULL)
      exit(EXIT_FAILURE);

    target = fopen(target_file, "w");

    while ((ch = fgetc(source)) != EOF)
      fputc(ch, target);

    source = fopen(source_file, "w");
    fclose(source);
    fclose(target);

    i++;
    sleep(10);
  }
}