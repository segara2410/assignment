#include <stdio.h>
#include <unistd.h>

int main()
{ 
  int value;

  if (!fork())
  {
    value = 0;
    printf("This is the child, the value is %d\n", value);
  }
  else
  {
    value = 1;
    printf("This is the parent, the value is %d\n", value); 
  }
}