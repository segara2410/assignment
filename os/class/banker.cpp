// Banker's Algorithm 
#include <iostream> 
using namespace std; 

int main() 
{
  int processes, resources;

  cout << "Number of process : ";
  cin >> processes;
  cout << "Number of resources : ";
  cin >> resources;

  int alloc[processes][resources], request[processes][resources];
  
  cout << "Current allocation matrix :\n";
  for (int i = 0; i < processes; i++)
  {
    for (int j = 0; j < resources; j++)
      cin >> alloc[i][j];
  }
  cout << "Request matrix :\n";
  for (int i = 0; i < processes; i++)
  {
    for (int j = 0; j < resources; j++)
      cin >> request[i][j];
  }

  int available_resources[resources];
  cout << "Available resources :\n";
  for (int i = 0; i < resources; i++)
    cin >> available_resources[i];
  
  int f[processes], ans[processes], ind = 0;
  for (int i = 0; i < processes; i++) 
    f[i] = 0;

  int finished_processes = 0;
  
  for (int i = 0; i < processes; i++)
  { 
    for (int j = 0; j < processes; j++)
    { 
      if (f[j] == 0) 
      { 
        int flag = 0; 
        for (int k = 0; k < resources; k++) 
        { 
          if (request[j][k] > available_resources[k])
          {
            flag = 1; 
            break; 
          } 
        } 

        if (flag == 0) 
        { 
          ans[ind++] = j; 
          for (int k = 0; k < resources; k++) 
            available_resources[k] += alloc[j][k]; 
          
          f[j] = 1;
          finished_processes = finished_processes + 1; 
        }
      } 
    } 
  }

  if (processes == finished_processes)
  {
    cout << "Following is the Safe Sequence" << endl; 
    for (int i = 0; i < processes; i++)
    {
      cout << "P" << ans[i];
      if (i != processes -1)
        cout << " -> "; 
      else
        cout << endl;
    }
  }
  else
  {
    cout << "There is deadlock" << endl;
    for (int i = 0; i < finished_processes; i++)
    {
      cout << "P" << ans[i];
      if (i != finished_processes -1)
        cout << " -> "; 
      else
        cout << " -> deadlock" << endl;
    }
  }

  return 0; 
}